<?php

if ( ! function_exists('array_group'))
{
    /**
     * Group an array by a specific key
     * @param array  $array
     * @param string $key
     * @return array
     */
    function array_group($array, $key)
    {
        $callback = is_callable($key);

        $grouped = [];

        foreach ($array as $value)
        {
            if ($callback)
            {
                $groupKey = $key($value);
            }
            else
            {
                $groupKey = is_object($value) ? $value->$key : $value[$key];
            }

            $grouped[$groupKey][] = $value;
        }

        return $grouped;
    }
}