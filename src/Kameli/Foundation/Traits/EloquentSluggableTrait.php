<?php namespace Kameli\Foundation\Traits;

use Str;

trait EloquentSluggableTrait {

    /**
     * Set the URL Alias
     * @param string $name
     * @param string $value
     * @param int $language_id
     */
    public function setSlugged($name, $value, $language_id = null)
    {
        $value = Str::slug($value);

        while ($this->causesSlugCollision($name, $value, $language_id))
        {
            if (preg_match('/\d+$/', $value, $matches))
            {
                $value = preg_replace('/\d+$/', ++$matches[0], $value);
            }
            else
            {
                $value .= '-1';
            }
        }

        $this[$name] = $value;
    }

    /**
     * Check if the slug causes collisions in the DB
     * @param string $name
     * @param string $value
     * @param int $languageId
     * @return bool
     */
    protected function causesSlugCollision($name, $value, $languageId = null)
    {
        $query = static::where($name, '!=', '')->where($name, $value);

        if ($languageId) $query->where('language_id', $languageId);

        if ($this->exists) $query->where('id', '!=', $this->id);

        return (bool) $query->count();
    }
} 