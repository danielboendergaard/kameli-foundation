<?php namespace Kameli\Foundation\Traits;

trait ImageTrait {

    public function __call($name, $arguments)
    {
        if (isset($this->sizes))
        {
            if (isset($this->sizes[$name]))
            {
                return $this->getFullPath($this->sizes[$name]);
            }

            if (in_array($name, $this->sizes))
            {
                return $this->getFullPath($name);
            }
        }

        return parent::__call($name, $arguments);
    }

    /**
     * Get the images in a specific size
     * @param string $size
     * @return string
     */
    public function getFullPath($size = '')
    {
        return implode('/', array_filter([$this->getPath(), $size, $this->getFilename()]));
    }

    /**
     * Get the path to the image files
     * @return string
     */
    abstract protected function getPath();

    /**
     * Get the filename
     * @return string
     */
    abstract protected function getFilename();
}