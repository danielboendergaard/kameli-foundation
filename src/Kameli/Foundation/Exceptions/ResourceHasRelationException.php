<?php namespace Kameli\Foundation\Exceptions;

class ResourceHasRelationException extends KameliException {

    protected $related;

    public function setRelated($related)
    {
        $this->related = $related;
    }

    public function getRelated()
    {
        return $this->related;
    }

}