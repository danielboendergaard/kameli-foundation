<?php namespace Kameli\Foundation\Exceptions;

use Exception;
use Illuminate\Mail\Message;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Contracts\MessageProviderInterface;

class ValidationException extends KameliException implements MessageProviderInterface {

    /**
     * @var MessageBag
     */
    protected $errors;

    /**
     * @param MessageProviderInterface|MessageBag|array|string $errors
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($errors, $message = 'Validation error', $code = 0, Exception $previous = null)
    {
        if ($errors instanceof MessageProviderInterface)
        {
            $this->errors = $errors->getMessageBag();
        }
        elseif ($errors instanceof MessageBag)
        {
            $this->errors = $errors;
        }
        else
        {
            $this->errors = new MessageBag((array) $errors);
        }

        parent::__construct($message, $code, $previous);
    }

    /**
     * Get the validation errors
     * @return MessageBag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Get the first error
     * @return string
     */
    public function getFirstError()
    {
        return $this->errors->first();
    }

    /**
     * Get the messages for the instance.
     * @return MessageBag
     */
    public function getMessageBag()
    {
        return $this->getErrors();
    }
}