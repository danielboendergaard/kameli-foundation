<?php namespace Kameli\Foundation\Exceptions;

class ResourceOwnershipException extends KameliException {}