<?php namespace Kameli\Foundation\Services;

use Closure;
use Illuminate\Cache\CacheManager;

class Cache {

    protected $cache;
    protected $cachekey;
    protected $minutes;

    public function __construct(CacheManager $cache, $cachekey, $minutes = null)
    {
        $this->cache = $cache;
        $this->cachekey = $cachekey;
        $this->minutes = $minutes;
    }

    /**
     * Retrieve data from cache
     * @param $key string
     * @return mixed
     */
    public function get($key)
    {
        return $this->cache->section($this->cachekey)->get($key);
    }

    /**
     * Add data to the cache
     * @param $key string
     * @param $value mixed
     * @param $minutes integer
     * @return mixed
     */
    public function put($key, $value, $minutes = null)
    {
        if (is_null($minutes)) $minutes = $this->minutes;

        return $this->cache->section($this->cachekey)->put($key, $value, $minutes);
    }

    /**
     * Test if item exists in cache, only returns true if exists && is not expired
     * @param $key string
     * @return bool
     */
    public function has($key)
    {
        return $this->cache->section($this->cachekey)->has($key);
    }

    /**
     * Get an item from the cache, or store the default value.
     *
     * @param  string   $key
     * @param  int      $minutes
     * @param  Closure  $callback
     * @return mixed
     */
    public function remember($key, $minutes, Closure $callback)
    {
        return $this->cache->section($this->cachekey)->remember($key, $minutes, $callback);
    }
} 