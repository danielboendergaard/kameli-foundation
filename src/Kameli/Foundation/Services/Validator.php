<?php namespace Kameli\Foundation\Services;

use Kameli\Foundation\Exceptions\ValidationException;
use Validator as BaseValidator;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Contracts\MessageProviderInterface;

abstract class Validator implements MessageProviderInterface {

    /**
     * The attributes of the object to be validated
     * @var array
     */
    protected $attributes;

    /**
     * The validation errors
     * @var MessageBag
     */
    protected $errors;

    /**
     * The id of the model to be validated
     * @var int|null
     */
    protected $modelId;

    /**
     * Local validation messages
     * @var array
     */
    protected $messages = [];

    /**
     * The custom attribute names
     * @var array
     */
    protected $attributeNames = [];

    /**
     * Start a new validation process
     * @param $attributes
     * @param int|null $id
     * @return static
     */
    public function create($attributes, $id = null)
    {
        $this->attributes = $attributes;

        $this->errors = new MessageBag;

        $this->modelId = $id;

        return $this;
    }

    /**
     * Alias for create
     * @param array $attributes
     * @param int|null $id
     * @return static
     */
    public function with($attributes, $id = null)
    {
        return $this->create($attributes, $id);
    }

    /**
     * Get the validation rules
     * @param int $id
     * @return array
     */
    abstract protected function rules($id = null);

    /**
     * Check if the validation passes
     * @return bool
     */
    public function passes()
    {
        $validation = BaseValidator::make($this->attributes, $this->rules($this->modelId), $this->getMessages());

        $validation->setAttributeNames($this->getAttributeNames());

        if ($validation->passes()) return ! $this->hasErrors();

        $this->addErrors($validation->errors()->getMessages());

        return false;
    }

    /**
     * Run the validation and throw exception on error
     * @param array $attributes
     * @param int $id
     * @throws \Kameli\Foundation\Exceptions\ValidationException
     */
    public function validate($attributes = null, $id = null)
    {
        if ( ! is_null($attributes))
        {
            $this->with($attributes, $id)->validate();
        }

        else
        {
            if ( ! $this->passes())
            {
                throw new ValidationException($this->getMessageBag());
            }
        }
    }

    /**
     * Get the validation messages
     * @return array
     */
    protected function getMessages()
    {
        $locale = app('config')->get('app.locale');

        if (isset($this->messages[$locale]) and is_array($this->messages[$locale]))
        {
            return $this->messages[$locale];
        }

        return $this->messages;
    }

    /**
     * Get the attribute names
     * @return array
     */
    protected function getAttributeNames()
    {
        $locale = app('config')->get('app.locale');

        if (isset($this->attributeNames[$locale]) and is_array($this->attributeNames[$locale]))
        {
            return $this->attributeNames[$locale];
        }

        return $this->attributeNames;
    }

    /**
     * Check if the validation fails
     * @return bool
     */
    public function fails()
    {
        return ! $this->passes();
    }

    /**
     * Add one or more errors to the validation object
     * @param array|string $messages
     */
    protected function addErrors($messages)
    {
        $this->errors->merge((array) $messages);
    }

    /**
     * Add an error to the validation object
     * @param $message
     */
    protected function addError($message)
    {
        $this->addErrors((array) $message);
    }

    /**
     * Check if any errors exist in the validation object
     * @return bool
     */
    protected function hasErrors()
    {
        return $this->errors->any();
    }

    /**
     * Get the messages for the instance.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getMessageBag()
    {
        return $this->errors;
    }
}