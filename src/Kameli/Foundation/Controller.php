<?php namespace Kameli\Foundation;

use Redirect;

class Controller extends \Illuminate\Routing\Controller  {

    protected function redirectMethod($method, $parameters = [], $status = 302, $headers = [])
    {
        return Redirect::action(get_class($this) . '@' . $method, $parameters, $status, $headers);
    }
}